﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class TransportShip : Ship
    {
        #region Attribute
        private ICollection<Resources> resources;
        private ICollection<Unit> units;
        #endregion

        #region Property
        public ICollection<Resources> Resources
        {
            get { return resources; }
            set { resources = value; }
        }
        
        public ICollection<Unit> Units
        {
            get { return units; }
            set { units = value; }
        }
        #endregion

        #region Constructor
        public TransportShip()
        {
            this.resources = new List<Resources>();
            this.units = new List<Unit>();
        }
        #endregion
    }
}
