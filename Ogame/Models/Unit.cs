﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class Unit : EntityBase
    {
        #region Attribute
        private String name;
        private int attackPoint;
        private int lifePoint;
        private int? planetId;
        private Planet planet;
        #endregion

        #region Property
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        [Range(0, 500)]
        public int AttackPoint
        {
            get { return attackPoint; }
            set { attackPoint = value; }
        }

        [Range(0, 1000)]
        public int LifePoint
        {
            get { return lifePoint; }
            set { lifePoint = value; }
        }

        [ForeignKey("Planet")]
        public int? PlanetId
        {
            get { return planetId; }
            set { planetId = value; }
        }

        public Planet Planet
        {
            get { return planet; }
            set { planet = value; }
        }
        #endregion

        #region Constructor
        public Unit()
        {

        }
        #endregion
    }
}
