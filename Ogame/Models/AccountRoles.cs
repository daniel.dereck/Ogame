﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    /// <summary>
    /// Roles class.
    /// </summary>
    public static class AccountRoles
    {
        /// <summary>
        /// Admin role.
        /// </summary>
        public static String ROLE_ADMIN = "Admin";

        /// <summary>
        /// Player role.
        /// </summary>
        public static String ROLE_PLAYER = "Player";
}
}