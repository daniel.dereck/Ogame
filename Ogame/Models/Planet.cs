﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class Planet : EntityBase
    {
        #region Static
        public static int lpMax = 1000000000;
        #endregion

        #region Attribute
        private String name;
        private Double size;
        private Type planetteType;
        private int lifePoint;
        private ICollection<Resources> resources;
        private ICollection<Building> buildings;
        private ICollection<Ship> ships;
        private ICollection<Unit> units;
        private int? playerId;
        private Player player;
        #endregion


        #region Properties
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        [Range(0, int.MaxValue)]
        public Double Size
        {
            get { return size; }
            set { size = value; }
        }

        public Type PlanetteType
        {
            get { return planetteType; }
            set { planetteType = value; }
        }

        [Range(0, 1000000000)]
        public int LifePoint
        {
            get { return lifePoint; }
            set { lifePoint = value; }
        }

        public ICollection<Resources> Resources
        {
            get { return resources; }
            set { resources = value; }
        }
        
        public ICollection<Building> Buildings
        {
            get { return buildings; }
            set { buildings = value; }
        }
        
        public ICollection<Ship> Ships
        {
            get { return ships; }
            set { ships = value; }
        }
        
        public ICollection<Unit> Units
        {
            get { return units; }
            set { units = value; }
        }

        [ForeignKey("Player")]
        public int? PlayerId
        {
            get { return playerId; }
            set { playerId = value; }
        }

        public Player Player
        {
            get { return player; }
            set { player = value; }
        }
        #endregion

        #region Constructor
        public Planet()
        {
            this.resources = new List<Resources>();
            this.buildings = new List<Building>();
            this.ships = new List<Ship>();
            this.units = new List<Unit>();
    }
        #endregion
    }
}
