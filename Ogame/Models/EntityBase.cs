﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public abstract class EntityBase
    {
        #region Attribute
        [Key]
        private int id;
        private DateTime createdAt;
        private DateTime updatedAt;
        #endregion

        #region Properties
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedAt
        {
            get { return createdAt; }
            set { createdAt = value; }
        }

        [Column(TypeName = "datetime2")]
        public DateTime UpdatedAt
        {
            get { return updatedAt; }
            set { updatedAt = value; }
        }
        #endregion

        #region methods
        public void OnBeforeInsert()
        {
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
        }

        public void OnBeforeUpdate()
        {
            this.UpdatedAt = DateTime.Now;
        }
        #endregion
    }
}
