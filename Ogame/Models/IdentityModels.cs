﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Ogame.Fixtures;

namespace Ogame.Models
{
    #region ApplicationUser
    // Vous pouvez ajouter des données de profil pour l'utilisateur en ajoutant d'autres propriétés à votre classe ApplicationUser. Pour en savoir plus, consultez https://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Notez que authenticationType doit correspondre à l'instance définie dans CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Ajouter des revendications d’utilisateur personnalisées ici
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    #endregion

    #region AdministrationDbContext
    /// <summary>
    /// User database.
    /// </summary>
    public class AdministrationDbContext : IdentityDbContext<ApplicationUser>
    {
        public AdministrationDbContext()
            :base("AdministrationConnection", throwIfV1Schema: false)
        {
            this.Database.CreateIfNotExists();
            if (!Database.CompatibleWithModel(false))
            {
                this.Database.Delete();
                this.Database.CreateIfNotExists();
            }
        }

        public static AdministrationDbContext Create()
        {
            AdministrationDbContext db = new AdministrationDbContext();
            #if DEBUG
            LoadFixtures(db);
            #endif
            return db;
        }

        public static void LoadFixtures(AdministrationDbContext db)
        {
            UserFixtures.Create(db);
        }
    }
    #endregion

    #region ApplicationDbContext
    /// <summary>
    /// Game database.
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            this.Database.CreateIfNotExists();
            if (!Database.CompatibleWithModel(false))
            {
                this.Database.Delete();
                this.Database.CreateIfNotExists();

            }
        }

        public static ApplicationDbContext Create()
        {
            ApplicationDbContext db = new  ApplicationDbContext();
            #if DEBUG
            LoadFixtures(db);
            #endif
            return db;
        }

        public static void LoadFixtures(ApplicationDbContext db)
        {
            ResourcesFixture.Create(db.Resources);
            db.SaveChanges();
            LevelFixture.Create(db.Levels);
            db.SaveChanges();
            PlanetFixture.Create(db.Planets);
            db.SaveChanges();
            UnitFixture.Create(db.Units);
            db.SaveChanges();
            ShipFixture.CreatFleet(db.Fleets);
            db.SaveChanges();
            ShipFixture.CreateBattleShip(db.BattleShips);
            db.SaveChanges();
            ShipFixture.CreateTrasportShip(db.TransportShips);
            db.SaveChanges();
            BuildingFixture.CreateCraftingBuilding(db.CraftingBuildings);
            db.SaveChanges();
            BuildingFixture.CreateHarvestBuilding(db.HarvestBuildings);
            db.SaveChanges();
            BuildingFixture.CreateSearchBuilding(db.SearchBuildings);
            db.SaveChanges();
        }

        #region DbSet
        public System.Data.Entity.DbSet<Ogame.Models.Planet> Planets { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Resources> Resources { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Level> Levels { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Unit> Units { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.TransportShip> TransportShips { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Ship> Ships { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.SearchBuilding> SearchBuildings { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.HarvestBuilding> HarvestBuildings { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Fleet> Fleets { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.CraftingBuilding> CraftingBuildings { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.BattleShip> BattleShips { get; set; }
        public System.Data.Entity.DbSet<Ogame.Models.Player> Players { get; set; }
        #endregion

        #region Methods
        public async override Task<int> SaveChangesAsync()
        {
            var changedEntities = ChangeTracker.Entries();

            foreach (var changedEntity in changedEntities)
            {
                if (changedEntity.Entity is EntityBase)
                {
                    var entity = (EntityBase)changedEntity.Entity;

                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.OnBeforeInsert();
                            break;

                        case EntityState.Modified:
                            entity.OnBeforeUpdate();
                            break;
                    }
                }
            }

            return await base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            var changedEntities = ChangeTracker.Entries();

            foreach (var changedEntity in changedEntities)
            {
                if (changedEntity.Entity is EntityBase)
                {
                    var entity = (EntityBase)changedEntity.Entity;

                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.OnBeforeInsert();
                            break;

                        case EntityState.Modified:
                            entity.OnBeforeUpdate();
                            break;
                    }
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().Property(e => e.Login);
        }
        #endregion
    }
    #endregion
}
