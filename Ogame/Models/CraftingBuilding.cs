﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class CraftingBuilding : Building
    {
        #region Attribut
        private ICollection<Building> buildings;
        private ICollection<Ship> ships;
        private ICollection<Unit> units;
        #endregion

        #region Property
        public ICollection<Building> Buildings
        {
            get { return buildings; }
            set { buildings = value; }
        }

        public ICollection<Ship> Ships
        {
            get { return ships; }
            set { ships = value; }
        }
        
        public ICollection<Unit> Units
        {
            get { return units; }
            set { units = value; }
        }
        #endregion

        #region Constructor
        public CraftingBuilding()
        {

        }
        #endregion
    }
}
