﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class Fleet : EntityBase
    {
        #region Attribute
        private String name;
        private ICollection<Ship> ships;
        #endregion

        #region Property
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        
        public ICollection<Ship> Ships
        {
            get { return ships; }
            set { ships = value; }
        }
        #endregion

        #region Constructor
        public Fleet()
        {
            this.ships = new List<Ship>();
        }
        #endregion
    }
}
