﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class Level : EntityBase
    {
        #region Attribut
        private String name;
        private ICollection<Resources> resources;
        #endregion

        #region Properties
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public ICollection<Resources> Resources
        {
            get { return resources; }
            set { resources = value; }
        }
        #endregion

        #region Constructor
        public Level()
        {
            this.resources = new List<Resources>();
        }
        #endregion
    }
}
