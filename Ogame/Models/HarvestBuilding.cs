﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class HarvestBuilding : Building
    {
        #region Attribut
        private Resources resources;
        #endregion

        #region Properties
        public Resources Resources
        {
            get { return resources; }
            set { resources = value; }
        }
        #endregion

        #region Constructor
        public HarvestBuilding()
        {

        }
        #endregion
    }
}
