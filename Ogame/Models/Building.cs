﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public abstract class Building : EntityBase
    {
        #region Static
        public static int lpMax = 10000000;
        #endregion

        #region Attribute
        private String name;
        private ICollection<Level> levels;
        private int lifePoint;
        private int productionSpeed;
        private int? planetId;
        private Planet planet;
        #endregion

        #region Property
        public String Name
        {
            get { return name; }
            set { name = value; }
        }
       
        public ICollection<Level> Levels
        {
            get { return levels; }
            set { levels = value; }
        }

        [Range(0, 10000000)]
        public int LifePoint
        {
            get { return lifePoint; }
            set { lifePoint = value; }
        }

        [Range(1, 10)]
        public int ProductionSpeed
        {
            get { return productionSpeed; }
            set { productionSpeed = value; }
        }

        [ForeignKey("Planet")]
        public int? PlanetId
        {
            get { return planetId; }
            set { planetId = value; }
        }

        public Planet Planet
        {
            get { return planet; }
            set { planet = value; }
        }
        #endregion

        #region Constructor
        public Building()
        {
            this.levels = new List<Level>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get lifepoint in percentage.
        /// </summary>
        /// <returns></returns>
        public float LifePointPercent()
        {
            float lp = 0;

            lp = (float)this.lifePoint * (float)100 / (float)lpMax;

            return lp;
        }
        #endregion
    }
}
