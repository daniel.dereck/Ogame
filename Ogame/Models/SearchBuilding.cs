﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class SearchBuilding : Building
    {
        #region Attribut
        private ICollection<Building> buildings;
        private ICollection<Ship> ships;
        #endregion

        #region Property
        public ICollection<Building> Buildings
        {
            get { return buildings; }
            set { buildings = value; }
        }
        
        public ICollection<Ship> Ships
        {
            get { return ships; }
            set { ships = value; }
        }
        #endregion

        #region Constructor
        public SearchBuilding()
        {
            this.buildings = new List<Building>();
        }
        #endregion
    }
}
