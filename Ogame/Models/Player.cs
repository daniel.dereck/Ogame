﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class Player : EntityBase
    {
        #region Attribute
        private String login;
        private ICollection<Planet> planets;
        private String userId;
        #endregion

        #region Property
        //        [Index(IsUnique = true)]
        public String Login
        {
            get { return login; }
            set { login = value; }
        }

        public ICollection<Planet> Planets
        {
            get { return planets; }
            set { planets = value; }
        }

        public String UserId
        {
            get { return userId; }
            set { userId = value; }
        }
        #endregion

        #region Constructor
        public Player()
        {
            this.planets = new List<Planet>();
        }
        #endregion
    }
}
