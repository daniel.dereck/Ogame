﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ogame.Models
{
    public class BattleShip : Ship
    {
        #region Attribute
        private int attackPoint;
        #endregion

        #region Property
        [Range(0, 1000)]
        public int AttackPoint
        {
            get { return attackPoint; }
            set { attackPoint = value; }
        }
        #endregion

        #region Constructor
        public BattleShip()
        {

        }
        #endregion
    }
}
