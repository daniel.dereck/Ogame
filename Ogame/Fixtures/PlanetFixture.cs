﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    public class PlanetFixture
    {
        public static void Create(DbSet<Planet> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    Planet entity = new Planet()
                    {
                        Name = "PlanetFixture" + i,
                        LifePoint = 90000,
                        Size = 90000,
                        PlanetteType = Models.Type.GLACIAL,
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
