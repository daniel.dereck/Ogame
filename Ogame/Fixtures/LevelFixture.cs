﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    public class LevelFixture
    {
        public static void Create(DbSet<Level> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    Level entity = new Level()
                    {
                        Name = "LevelFixture",
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
