﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    public static class ResourcesFixture
    {
        public static void Create(DbSet<Resources> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    Resources entity = new Resources()
                    {
                        Name = "ResourcesFixture",
                        Quantity = 5000,
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
