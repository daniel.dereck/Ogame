﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    /// <summary>
    /// Fixture class for user.
    /// </summary>
    public static class UserFixtures
    {
        /// <summary>
        /// RoleManager.
        /// </summary>
        public static RoleManager<IdentityRole> roleManager;

        /// <summary>
        /// UserManager.
        /// </summary>
        public static UserManager<ApplicationUser> userManager;

        /// <summary>
        /// Number item generate
        /// </summary>
        public static int NUMBER = 2;

        /// <summary>
        /// Create fixtures;
        /// </summary>
        /// <param name="db"></param>
        public static void Create(AdministrationDbContext db)
        {
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            if (db.Roles.Count() == 0 && db.Users.Count() == 0)
            {
                CreateRoles();
                db.SaveChanges();
                CreateUsers();
                db.SaveChanges();
            }
        }

        private static void CreateRoles()
        {
            if (!roleManager.RoleExists(AccountRoles.ROLE_ADMIN))
            {
                CreateRole(roleManager, AccountRoles.ROLE_ADMIN);
            }

            if (!roleManager.RoleExists(AccountRoles.ROLE_PLAYER))
            {
                CreateRole(roleManager, AccountRoles.ROLE_PLAYER);
            }

        }

        private static void CreateUsers()
        {
            if (roleManager.RoleExists(AccountRoles.ROLE_ADMIN))
            {
                CreateUser("Admin", "Admin@ogame.com", "123P@ssword", AccountRoles.ROLE_ADMIN);
                CreateUser("Dereck", "daniel.dereck@gmail.com", "123P@ssword", AccountRoles.ROLE_ADMIN);
                CreateUser("Kevin", "denouillekevin@gmail.com", "123P@ssword", AccountRoles.ROLE_ADMIN);
            }

            if (roleManager.RoleExists(AccountRoles.ROLE_PLAYER))
            {
                CreateUser("User", "player@exemple.com", "123P@ssword", AccountRoles.ROLE_PLAYER);

                for (int i = 0; i < NUMBER; i++)
                {
                    CreateUser("UserFixture" + i, "UserFixture" + i + "@fixture.com", "123P@ssword", AccountRoles.ROLE_PLAYER);
                }
            }
        }

        private static void CreateRole(RoleManager<IdentityRole> roleManager, String roleName)
        {
            if (!roleManager.RoleExists(roleName))
            {
                var role = new IdentityRole();
                role.Name = roleName;
                roleManager.Create(role);
            }
        }

        private static void CreateUser(String username, String email, String password, String role)
        {
            ApplicationUser user = new ApplicationUser();

            user.UserName = username;
            user.Email = email;

            var chkUser = userManager.Create(user, password);

            if (chkUser.Succeeded)
            {
                //Create player when we create user
                ApplicationDbContext gameContext = new ApplicationDbContext();
                if (gameContext != null)
                {
                    gameContext.Players.Add(new Player()
                    {
                        UserId = user.Id,
                        Login = user.UserName,                        
                    });
                    gameContext.SaveChanges();
                }

                var result1 = userManager.AddToRole(user.Id, role);
            }
        }
    }
}