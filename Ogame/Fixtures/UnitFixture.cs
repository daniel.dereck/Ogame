﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    /// <summary>
    /// Unit fixture class.
    /// </summary>
    public class UnitFixture
    {
        /// <summary>
        /// Create fixtures.
        /// </summary>
        /// <param name="dbSet"></param>
        public static void Create(DbSet<Unit> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    Unit entity = new Unit()
                    {
                       AttackPoint = 50,
                       LifePoint = 500,
                       Name = "Peon",
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
