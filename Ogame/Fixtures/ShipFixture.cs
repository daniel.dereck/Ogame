﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    /// <summary>
    /// Ship fixture class.
    /// </summary>
    public class ShipFixture
    {
        /// <summary>
        /// Create fleet fixture.
        /// </summary>
        /// <param name="dbSet"></param>
        public static void CreatFleet(DbSet<Fleet> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                Fleet fleet = new Fleet()
                {
                    Name = "FleetFixture",
                };

                for (int i = 0; i < numberGenerate; i++)
                {
                    Ship ship;

                    if (i % 2 == 0)
                    {
                        ship = new BattleShip()
                        {
                            Name = "BattleShipFixture" + i,
                            AttackPoint = 666,
                            LifePoint = 90000,
                        };
                    }
                    else
                    {
                        ship = new TransportShip()
                        {
                            Name = "TrasportShipFixture" + i,
                            LifePoint = 90000,
                        };
                    }
                    fleet.Ships.Add(ship);
                }
                dbSet.Add(fleet);
            }
        }

        /// <summary>
        /// Create battle ship fixtures.
        /// </summary>
        /// <param name="dbSet"></param>
        public static void CreateBattleShip(DbSet<BattleShip> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    BattleShip entity = new BattleShip()
                    {
                        Name = "BattleShipFixture" + i,
                        AttackPoint = 666,
                        LifePoint = 90000,
                    };

                    dbSet.Add(entity);
                }
            }
        }

        public static void CreateTrasportShip(DbSet<TransportShip> dbSet)
        {
            if (dbSet.Count() == 0)
            {
                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    TransportShip entity = new TransportShip()
                    {
                        Name = "TrasportShipFixture" + i,
                        LifePoint = 90000,
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
