﻿using Ogame.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ogame.Fixtures
{
    public static class BuildingFixture
    {
        public static void CreateCraftingBuilding(DbSet<CraftingBuilding> dbSet)
        {
            if (dbSet.Count() == 0)
            {
                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    CraftingBuilding entity = new CraftingBuilding()
                    {
                        PlanetId = 1,
                        Name = "CraftingBuildingFixture" + i,
                        ProductionSpeed = 1,
                        LifePoint = 10000,
                    };

                    dbSet.Add(entity);
                }
            }
        }

        public static void CreateHarvestBuilding(DbSet<HarvestBuilding> dbSet)
        {
            if (dbSet.Count() == 0)
            {
                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    HarvestBuilding entity = new HarvestBuilding()
                    {
                        PlanetId = 1,
                        Name = "HarvestBuildingFixture" + i,
                        ProductionSpeed = 1,
                        LifePoint = 10000,
                    };

                    dbSet.Add(entity);
                }
            }
        }

        public static void CreateSearchBuilding(DbSet<SearchBuilding> dbSet)
        {
            if (dbSet.Count() == 0)
            {

                int numberGenerate = 2;

                for (int i = 0; i < numberGenerate; i++)
                {
                    SearchBuilding entity = new SearchBuilding()
                    {
                        PlanetId = 1,
                        Name = "SearchBuildingFixture" + i,
                        ProductionSpeed = 1,
                        LifePoint = 10000,
                    };

                    dbSet.Add(entity);
                }
            }
        }
    }
}
