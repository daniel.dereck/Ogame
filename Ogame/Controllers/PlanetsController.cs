﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ogame.Models;

namespace Ogame.Controllers
{
    public class PlanetsController : ApplicationBaseController<Planet>
    {
        public ActionResult PlanetPlayer ()
        {
            var  item = dbContext.Planets.ToList().Where(x => x.PlayerId == null);
            return View(item);

        }

        public ActionResult GetAllById(int? id)
        {
            List<Planet> items = (List<Planet>)dbContext.Planets.ToList().Where(x => x.PlayerId == id);
            return View(items);
        }
    }
}
