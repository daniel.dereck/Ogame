﻿using Ogame.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Ogame.Controllers
{
    public class ApplicationBaseController<T> : Controller where T : EntityBase, new()
    {
        protected ApplicationDbContext dbContext = new ApplicationDbContext();

        public virtual ActionResult Index()
        {
            List<T> items = dbContext.Set<T>().ToList();
            return View(items);
        }

        /* Pagination
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DateSortParm = sortOrder == "CreatedAt";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            IEnumerable<T> items = dbContext.Set<T>().ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                items = items.Where(x => x.Id.Equals(int.Parse(searchString)));
            }
            switch (sortOrder)
            {
                case "Date":
                    items = items.OrderBy(x => x.CreatedAt);
                    break;
                default:  // Name ascending 
                    items = items.OrderBy(x => x.Id);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(items.ToPagedList(pageNumber, pageSize));
        }
        */

        [HttpGet]
        public virtual async Task<ActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Create([Bind(Include = "")]T item)
        {
            if (ModelState.IsValid)
            {
                dbContext.Set<T>().Add(item);
                await dbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpGet]
        public virtual async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> DeleteConfirmed(int? id)
        {
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item != null)
            {
                dbContext.Set<T>().Remove(item);
                await dbContext.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit([Bind(Include = "")] T item)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(item).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [HttpGet]
        public virtual async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}