﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Ogame.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Ogame.Controllers
{
	[Authorize]
	public class UsersController : Controller
    {
		// GET: Users
		public Boolean isAdminUser()
		{
			if (User.Identity.IsAuthenticated)
			{
				var user = User.Identity;
				AdministrationDbContext context = new AdministrationDbContext();
				var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				var s = UserManager.GetRoles(user.GetUserId());
				if (s[0].ToString() == AccountRoles.ROLE_ADMIN)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}

		public ActionResult Index()
		{
            if (User.Identity.IsAuthenticated)
			{
                ApplicationDbContext context = new ApplicationDbContext();

                var user = User.Identity;
				ViewBag.Name = user.Name;

				ViewBag.displayMenu = "No";
                String uid = user.GetUserId();
                ViewBag.Player = context.Players.FirstOrDefault(x => x.UserId == uid);


				if (isAdminUser())
				{
					ViewBag.displayMenu = "Yes";
				}
				return View();
			}
			else
			{
				ViewBag.Name = "Not Logged IN";
			}


			return View();


		}
	}
}